# Månejæger

![TODO: Skift det her billede ud](img/temp.png)

Månejægeren er en arketype som kan spilles på henholdsvis to forskellige
måder.
De kan på den ene side fungere som en trænet jæger som benytter sig af
langdistance våben som buer og armbrøst, samt en række magiske måne
besværgelser.
Eller som en listig tyv hvis speciale er at snige sig om bag
modstanderen og give dem et dødeligt rygstik.
Udgangspunktet for Månejægeren er, at de benytter sig af Månens lys til
at bedregøre deres jagtevner.
Nogle tilbeder månen fromt andre ser blot Månen som en kilde til energi,
som kan og bør benyttes til egen vinding.
De er adrætte, snedige og perceptive karakterer. Styrke, Tro og
Bevægelighed er bedst for en Månejæger.
En karakter som ønsker at spille Månejæger skal vælge enten Jægerns
eller Tyvens vej når de kommer i level 2.

Indtil da fungerer Månetyv Tabellen som din tabel, men hvis du vælger
Månejæger Vejen skal du konsultere Månejæger tabellen.

**Moralsk overbevisning:** Kaotisk God, Neutral God, Neutral eller
Neutral Ond.

**HP Terning:** 1d8

## Klasse kompetencer

Månetyven/jægerens klasse kompetencer er Akrobatik(Bevæg), Vurder
Værdigenstand(Int), Bluff (Kar), Klatre (Styr), Diplomati (Kar),
Deaktiver fælde (Bevæg), Flugt Mester (Bevæg) Intimidér(Kar), Viden
(Natur) (Int), Viden (lokal) (Int)
Perception (Int), List(Bevæg), Svømning (Styr).

## Månetyv Tabel

Level   | Angrebs Bonus | Viljestykre | Refleks | Kropsstyrke | Særlige egenskaber                     | Daglige besværgelser
------- | -------       | -------     | ------- | -------     | -------                                |
1       | +1            | +0          | +2      | +0          |                                        | 1+Kar Modifier
2       | +1            | +0          | +3      | +0          | Valg af Vej                            | 2+Kar Modifier
3       | +2            | +1          | +3      | +1          | Speciale, Bonus Speciale: *Knivmester* | 2+Kar Modifier
4       | +3            | +1          | +4      | +1          | BS Point                               | 3+Kar Modifier
5       | +4            | +2          | +4      | +2          | Speciale, Bonus Kamp Speciale          | 4+Kar Modifier
6       | +4            | +3          | +5      | +3          |                                        | 5+Kar Modifier
7       | +5            | +3          | +6      | +3          | Speciale                               | 5+Kar Modifier
8       | +6            | +4          | +7      | +4          | BS Point                               | 6+Kar Modifier
9       | +7            | +5          | +7      | +4          | Speciale                               | 6+Kar Modifier
10      | +8            | +5          | +8      | +5          | Speciale, BS Point                     | 7+Kar Modifier

## Månetyv Særlige Egenskaber

### Rygstik

Tyvens primære evne til at skade sine modstandere, kan kun udføres
mens modstanderen står med ryggen til tyven, rygstikket giver ens normale
våben skade +50 %

Hvis rygstikket er et kritisk slag tredobles skaden. I level 3 øges
rygstiks skaden til 75% af våben skaden og i level 6 til 100%.

### Bonus Kamp Speciale Omgåelse

I level 3 får tyven **Bonus Kamp Specialet Omgåelse** dette øger
tyvens bevægelighed med +1 i 1 runde + ½ level.

### Lommetyv

I level 4 får Tyven **Bonus Specialet Lommetyv,** dette giver +4 når
de laver et Kunstgrebs tjek.

### Snigangreb

I level 4 kan Månetyven i kombination med et rygstik lave et
snigangreb. Dette kræver et Listigheds tjek imod modstanderens
perceptions tjek.

Klares tjekket må Månetyven lægge 2d6 til deres skade.

### Bonus Kamp Speciale

I level 5 får Månetyven et ekstra valgfrit Kamp Speciale.

### Pulsstik

I level 6 får Månetyven evnen til at lave et præciseret
knivstik mod en modstanders pulsåre Hvis Pulstikket rammer på angrebs
tjekket er det automatisk et kritisk slag. Er angrebs tjekket et kritisk
slag 3 dobles skaden.

## Månejæger Tabel

Level   | Angrebs Bonus | Viljestykre | Refleks | Kropsstyrke | Særlige egenskaber                                            | Daglige besværgelser
------- | -------       | -------     | ------- | -------     | -------                                                       |
1       | +1            | +0          | +2      | +0          | Månepil                                                       | 1+Kar Modifier
2       | +1            | +0          | +2      | +0          |                                                               | 2+Kar Modifier
3       | +2            | +1          | +3      | +1          | Speciale, Bonus Speciale: Buemester Månepil Månelys Magiskole | 2+Kar Modifier
4       | +3            | +1          | +4      | +1          | BS Point                                                      | 3+Kar Modifier
5       | +4            | +2          | +4      | +2          | Speciale Månepil                                              | 4+Kar Modifier
6       | +4            | +3          | +5      | +3          |                                                               | 5+Kar Modifier
7       | +5            | +3          | +6      | +3          | Speciale Månepil                                              | 5+Kar Modifier
8       | +6            | +4          | +7      | +4          | BS Point                                                      | 6+Kar Modifier
9       | +7            | +5          | +7      | +4          | Speciale Månepil                                              | 6+Kar Modifier
10      | +8            | +5          | +8      | +5          | Speciale BS Point Månepil                                     | 7+Kar Modifier

## Månejæger Særlige Egenskaber

### Månepil

I level 1 har Månejægeren adgang til besværgelsen Månepil, han/hun
fremmaner en pil af månelys som de skyder afsted med deres bue.
Pilen penetrerer og påvirker fjenden med en ud af fire effekter.

Kun én af de fire effekter kan vælges fra level 1. Hvert andet level
forøges effekten eller Månejægeren kan vælge endnu en effekt at påføre
pilen.

#### Månepil skade
Månepilen penetrerer fjendens rustning og skader 1d6 + Jægerens våben skade. Pilen fungerer som en magisk besværgelse og skal    derfor ramme modstanderens Berørings RK. Pilens skade stiger med 1d6 hvert andet level til en max skade på 6d6 i level 10. En Månejæger kan bruge disse magiske pile et antal gange om dagen svarende til deres karisma-modifier + level. Ved et kort hvil kan man restore halvdelen af sine daglige magiske pile.

####  Månepil gift

Månepilen påføres en magisk gift som, hvis den
rammer, forgifter modstanderen med -1
konstitution -1 bevægelighed og 1 skade hver
runde i 3 runder.
                                              
Effekten af giften forøges med 1 hvert andet
level og til en max på -7 konstitution -7
bevægelighed og 7 skade hver runde i level 10.
                                              
Modstanderen kan forsøge at bekæmpe giftens
effekt ved et kropsstyrk tjek svarende til
Månejægerens Besværgelses Niveau.


#### Månepil Lammelse 

Månepilen lammer modstanderen med en magisk   
aura som gør dem ude afstand til at bevæge sig
eller angribe i 1 runde.                      
                                              
Modstanderen skal klare et Kropsstyrketjek    
svarende til Månejægerens Besværgelses Niveau 
for at undgå at blive lammet.                 
                                              
Effekten forøges med 1 runde hvert andet leve.

#### Månepil Hovedskud

Månepilen sigtes mod modstanderens hoved, hvis 
angrebs tjekket klares er dette skud
automatisk et kritisk slag.
                                             
Månejægeren kan lave et hovedskud samme antal  
gange svarende til deres karisma-modifier +    
level pr dag.                                  


### Spor

I level 1 kan Månejægeren spore en type væsner baseret
på, hvilken evne han/hun har valgt. Spor virker kun om natten og når den
er aktiveret kan et lysende spor af månelyse ses langs jorden, ledende
månejægeren mod det nærmeste af det valgte type væsen han/hun ønsker at
spore.

Spor            | Egenskab
--------------- | ---------------------------------------------------------------------------------------------------------------------
Spor Menneske   | Månejægeren kan spore alle menneskeligende væsner i nærheden.
Spor Dyr        | Månejægeren kan spore alle ikke magiske dyr i nærheden.
Spor Magi       | Månejægeren kan spore alle magiske væsner og kilder i nærheden.
Spor Udød       | Månejægeren kan spore alle Udøde væsener i nærheden.
Spor Element    | Månejægeren kan spore alle element kilder, ild, vand, jord og vind samt element væsner som f.eks Kæmper i nærheden.

### Månefælde

I level 2 kan Månejægeren plante fælder skjult på jorden, fælderne har
forskellige egenskaber men bruges til at uskadeliggøre fjender og vildt.

Fælder           | Egenskab
---              | ---
Bjørnefælde    | Månejægeren kan lave en fælde, som kan lamme alle medium store og små dyr, lammelsen varer i 3 runder og giver -1 bevægelighed pr runde. Dyret kan lave et Kropsstyrketjek for at undgå lammelsen.
Faldlem        | Månejægeren graver et hul i jorden 3 meter dybt og dækker det med blade og kviste. Alle medium store og små væsner som falder ned i hullet er fanget, og skal lave et bevægeligheds tjek for at komme ud.
Giftfælde      | Månejægeren placerer et par små gift dartpile i jorden, alle væsner som træder på pilerne bliver forgiftet og skal klare et Kropstyrke tjek for at undgå forgiftning, giften varer i 3 runder og giver 4 skade pr runde. Dartpilerne kan også kastes istedet for at plantes som en fælde.

## Bonus Kamp Speciale Buemester

I level 3 får Månejægeren **specialet Buemester,** dette speciale
øger **s**kaden og angrebs tjek med buer og andre langdistance våben med
+2 (ikke pistoler, geværer eller bomber).

## Besværgelser

I level 3 får Månejægeren adgang til alle level 1-3 besværgelser i
Månelys magiskolen.

## Pileregn

I level 5 kan Månejægeren fremmane en regn af magiske pile af månelys
som falder lodret fra himmelen i et område på 3 meter , alle inde for
området taget 3d6 i skade.
Skaden forsøges med 1d6 hvert 2 level til en max skade på 7d6 i level
10. En modstander kan lave et refleks tjek svarende til Månejægerens
Besværgelses Niveau for at halvere skaden.


## Åndedyr Medium

I level 6 kan Månejægeren påkalde et medium stort dyr af ren månelys
som hjælper ham/hende i kamp. Åndedyret angriber som var det en
berørings-magi og har tilstanden inkorporlig, hvilket betyder at det
ikke kan angribes med fysiske genstande.
Hvilke typer angreb og evner dyret har afgøres af hvilket slags dyr
Månejægeren vælger.

I level 10 kan Månejægeren fremmane et Stort Åndedyr.
