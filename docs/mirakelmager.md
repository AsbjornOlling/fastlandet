# Mirakelmageren

![TODO: Skift det her billede ud](img/temp.png)

Mirakelmageren er en rettroende og godsindet magiker fra Solslægten.
De er ihærdige, trofaste og godhjertede mennesker som følger Sol og Hans
Kirke.

Deres primære roller i Solriget er som præste for Solkirken eller
missionærer som vandrer Fastlandet rundt og prædiker Sols Budskab.
De undgår for det meste kamp og vold men skulle det ske at de står
overfor en aggressive fjende er deres bedste tricks at forvirre fjenden
og beskytte dem selv og deres holdkammerater med Solbesværgelser.
Mirakelmagere fungerer oftest som præster i Solkirken eller som
missionærer som vandrer Fastlandet rundt og spreder Sols Budskab.


**Moralsk overbevisning**: Neutral God eller Lovfuld God

**HP Terning:** 1d6

## Klassekompetencer

Mirakelmagerens klassekompetencer er 

- Diplomati (kar)
- Førstehjælp(Int)
- Viden (Magi) (Int)
- Viden (Historie) (Int)
- Viden (religion) (Tro)
- Lingvistik (Int)
- Detekt Magi (Int)

## Mirakelmager Tabel

Level   | Angrebs Bonus | Viljestyrke | Refleks | Kropsstyrke | Særlige egenskaber       | Daglige besværgelser
------- | -------       | -------     | ------- | -------     | -------                  |
1       | +0            | +2          | +0      | +2          | Hellig Aura              | 1+Tros Modifier
2       | +1            | +3          | +0      | +3          | Helligt Lys              | 2+Tros Modifier
3       | +1            | +4          | +1      | +3          | Hellig Aura +1, Speciale | 3+Tros Modifier
4       | +2            | +4          | +1      | +4          | BS Point, Ny Magiskole   | 4+Tros Modifier
5       | +3            | +5          | +1      | +4          | Hellig Aura +1, Speciale | 4+Tros Modifier
6       | +4            | +5          | +2      | +5          |                          | 5+Tros Modifier
7       | +4            | +6          | +3      | +5          | Hellig Aura +1, Speciale | 6+Tros Modifier
8       | +5            | +7          | +3      | +6          | BS Point, Ny Magiskole   | 7+Tros Modifier
9       | +6            | +8          | +4      | +7          | Hellig Aura +1, Speciale | 8+Tros Modifier
10      | +7            | +9          | +5      | +8          | Hellig Aura +1, Speciale | 9+Tros Modifier

## Særlige Egenskaber

### Hellig Aura

I level 1 kan Mirakelmageren vælge mellem 1 ud af 4 Auras som de kan
kaste op til 3 gange om dagen + Deres Tros- modifier, en auras effekt
varer 1 time.
Auran kan kastes på Mirakelmageren selv eller en allieret.
Auraens effekt forstærkes med +1 i level 3,5,7,9 og 10.

#### Hellig Aura Fromhed

Denne aura giver alle som bærer den +2 i Tro og alle Trosbaserede tjeks                 

#### Hellig Aura Renhed

Denne aura giver alle som bærer den +2 i konstitution og konstitutions baserede tjeks   

#### Hellig Aura Karisma

Denne aura giver alle som bærer den +2 Karisma og Karisma tjeks.

#### Hellig Aura Visdom

Denne aura giver alle som bærer den +2 I intelligens og intelligens tjeks.

## Besværgelser

I level 1 har Mirakelmageren adgang til besværgelserne fra Sollys Magiskolen.

### Helligt lys

I level 2 får Mirakelmageren evnen Hellig Lys. Denne besværgelse kan
kastes på hvem som helst og helbreder 1d6 HP. Denne evne forøges med 1d6
hvert level til et max antal på 9d6 i level 10. Helligt Lys kan kastes
op til 3 gange om dagen + Mirakelmagerens Tros-modifier

**Specialet Rettroende**: Du kan lægge din Tros-modifier til dine helbredelser.

**Specialet Helbredes Hænder** øger terningeslaget til 1d8.
Hvis modtageren af helbredelsen har **Specialet Solskind** kan de lægge
1d4 ekstra til helbredelsen.

### Troens Skjold

I level 5 kan Mirakelmageren som en almindelig handling fremmane en
lysende beskyttende aura om sig selv eller en allieret. Auraen giver +2
til RK og yderligere +1 for hvert 3 Mirakelmager level, til en samlet
max bonus på +6 i level 10. Skjoldet varer i 1d3 runder.

**Specialet Rettroende:** Gør denne besværgelse til en Hurtig Handling.


### Sol Frygt

I level 6 kan Mirakelmageren påføre Sol frygt på en fjende, dette vil
blænde fjenden hvis de er i nærheden af en solrig lyskilde. Besværgelsen
kan afværges ved et Viljestyrke tjek svarende til Mirakelmagerens
Besværgelses Niveau.


### Sols Visdom

I level 6 kan Mirakelmageren låne visdom fra Sol og få
+4 til sin intelligens i 1 min pr level. Denne evne kræver et Trostjek.

### Sols Velsignelse

I level 8 kan Mirakelmageren med sin Tro bønfalde Sol om at genoplive
en afdød, denne evne kan kun bruges 1 gang om dagen og skal benyttes
inden for 1 timer af en persons død. Denne evne kræver et meget højt
Trostjek.
