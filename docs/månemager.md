# Månemager

Månemageren er en magiker fra Måneslægten som benytter sig af Månens lys
til at kunne fremmane besværgelser.
De er gode i kamp på afstand og kan med deres mange forbandelser og
lammelser gøre en modstander totalt ukampdygtig.
De er som oftest fromme i deres tillid til Månen og ser "Hende" som en
kilde til visdom og harmoni, dog kan de også være mere interesseret i
magiens egenskaber og potentiale end i selve Månen, ikke alle Månemagere
er fromme men i sidste ende trækker de deres magiske kræfter fra Månens
lys.
Tro og Intelligens er bedst for dem.

**Moralsk Overbevisning:** Kaotisk God, Neutral eller Neutral God.

**HP Terning:** 1d6
**
Klasse kompetencer:**

Månemagerens klasse kompetencer er vurder værdigenstand(Int), Viden
(Magi) (Int), Viden (Historie), Viden (religion) Håndværk(Alkymi), Brug
magisk redskab (Int), Detekt Magi (int)

## Månemager Tabel

Level   | Angrebs Bonus | Viljestykre | Refleks | Kropsstyrke | Særlige egenskaber                  | Daglige besværgelser
------- | -------       | -------     | ------- | -------     | -------                             |
1       | +0            | +2          | +0      | +0          | Magiskole (Månelys Månesegl)        | 1+Tros Modifier      |
2       | +0            | +3          | +0      | +0          | Måneforbandelse                     | 2+Tros Modifier      |
3       | +1            | +3          | +1      | +1          | Speciale                            | 3+Tros Modifier      |
4       | +1            | +4          | +1      | +1          | BS Point, Ny Magiskole              | 4+Tros Modifier      |
5       | +2            | +4          | +1      | +2          | Speciale Månesegl                   | 5+Tros Modifier      |
6       | +3            | +5          | +2      | +3          | Bonus Speciale: Døsende Besværgelse | 6+Tros Modifier
7       | +3            | +6          | +2      | +4          | Speciale                            | 7+Tros Modifier      |
8       | +4            | +7          | +3      | +4          | BS Point                            | 7+Tros Modifier      |
9       | +5            | +8          | +4      | +5          | Speciale, Ny Magiskole, Månesegl    | 8+Tros Modifier      |
10      | +5            | +9          | +4      | +5          | Speciale, Bonus Speciale: Månebarn  | 9+Tros Modifier      |

## Månesegl

I level 1 kan Månemageren vælge mellem 3 forskellig Måneselg som kan
bruges imod en modstander, kun ét kan være aktivt på den samme
modstander adgangen.
Effekten af hvert segl fordobles hvert 4 level.

Mageren kan bruge op til 3 segl + sin Tros-modifier om dagen.

**Specialet Rettroende:** Giver Månemageren lov til at lægge sin Tros-modifier til effekterne.

### Månesegl Død

Dette segl tager 1 runde at kaste og tæller som en Fuld-runde handling, den som bærer Månesegl Død skal trække 10% fra deres samlede HP. Effekten varer indtil modstanderen dør eller seglet fjernes.

### Månesegl Sygdom

Dette segl giver bæreren -2 til Konstitution og Bevægelighed. Effekten varer indtil modstanderen dør eller seglet fjernes.

### Månesegl Svaghed

Dette segl giver bæreren -2 i Styrke. Effekten varer indtil modstanderen dør eller seglet fjernes.

## Besværgelser

I level 1 har Månemageren adgang til magiskolen Månelys.

### Måneforbandelser

I level 2 Kan Månemageren fremkalde en række forbandelser over sine
modstandere. En modstander kan forsøge at undgå at blive påvirket af
forbandelsen ved at lave et Viljestyrke Tjek svarende til Månemagerens
Besværgelses Niveau.

#### Forbandelse Nærsyn

Modstanderens syn svækkes så de får -2 til alle angrebs og perceptions tjeks. Varer i 1 runder + ½ level eller indtil effekten fjernes.
I level 6 gør forbandelsen modstanderen blind i stedet for nærsynet.

#### Forbandelse Uheld

Modstanderen rammes af frygtelig uheld, det næste tjek som modstanderen skal lave skal kastes 2 gange og det laveste af de to slag skal vælges.

#### Forbandelse Natterror

Modstanden skal klare et viljestyrke tjek svarende til Månemagerens Besværgelses Niveau, hvis det fejles falder de i en dyb søvn med horrible mareridt. Søvnen varer i 1 runde men når modstanderen vågner skal de klare et viljestyrke tjek igen for ikke at blive paniske.

### Bonus Meta Magi Speciale: Døsende Besværgelse

I level 5 får Månemageren automatisk Meta Magi **Specialet Døsende
Besværgelse.

En døsende besværgelse har muligheden for at påføre Døsigheds
effekten på en modstander hvis besværgelsen som kastes er et kritisk
slag. Dette vil sige at effekten kun virker på de besværgelser som
kræver et angrebs tjek.

### Transformer Familiar

Hvis Månemageren er i besiddelse af en Medium eller Lille
familiar kan de I level 9 bruge en fuld-runde handling på at forvandle
deres familiar til en Uglebjørn.
Din familiar forbliver en Uglebjørn i 1 time eller indtil den slås
ihjel.

### Bonus Speciale Månebarn

I level 10 får Månemageren det unikke **speciale Månebarn**. Som
Månebarn er man velsignet af Månen selv og denne velsignelse giver
Månemageren mulighed for at helbrede sår og skader ved at tilbringe 1
time i Månens lys. Månemangeren helbredes for 3d6 pr time de står i rent
Månelyse. Dette lys skal komme fra Månen selv og ingen af Månemågerens
besværgelser kan bruges som erstatning. Derudover bliver Månemageren
komplet immun overfor alle besværgelser som har blindheds effekten.
