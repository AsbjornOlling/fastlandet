# Månesiger

![TODO: Skift det her billede ud](img/temp.png)

Månesigeren er en magiker fra Måneslægten som har viet sit liv til
Naturen og dens skabninger.

Da Månen var med til at skabe verdens natur ligger Hendes kraft immanent
i hele naturen
Månesigeren bruger derfor sine evner til at kommunikere med naturen og
søger altid at balancere forholdet mellem menneske og natur.
Dette involverer dog ikke elementerne, da de i sig selv er adskilt fra
den levende natur.
Månesigeren har som sagt et nært forhold til naturen og dens væsener. De
kan helbrede sygdomme, fjerne lammelser, forbandelser og forvandle sig
til forskellig dyr.

**Moralsk Overbevisning:** Neutral God, Kaotisk God eller Neutral.

**HP Terning:** 1d8

## Klasse Kompetencer

Månesigerens klasse kompetencer er 

- Klatret (Styr)
- Håndter dyr (Kar),
- Førstehjælp(Int)
- Viden (Natur) (Int)
- Perception (int)
- Ride (Bevæg)
- Overlevelse (Int) 
- Svøm (Styr).
- Detekt Magi (Int)
- Diplomati (Kar)

## Månesiger Tabel

Level | Angrebs Bonus | Viljestyrke | Refleks | Kropstyrke | Særlige Egenskaber                                     | Daglige Besværgelser |
----- | -------       | -------     | ------- | -------    | -------                                                | -------              |
| 1     | +1      | +2      | +0      | +2      | Formskift, Månelys Magiskole      | 1+Kar Modifier  |
| 2     | +1      | +3      | +0      | +3      |         | 2+Kar Modifier  |
| 3     | +2      | +4      | +1      | +4      | Formskift, Speciale     | 3+Kar Modifier  |
| 4     | +3      | +4      | +1      | +4      | BS point      | 4+Kar Modifier  |
| 5     | +3      | +5      | +1      | +5      | Speciale       | 4+Kar Modifier  |
| 6     | +4      | +5      | +2      | +5      | Formskift, Ny magiskole     | 5+Kar Modifier  |
| 7     | +4      | +6      | +3      | +6      | Speciale       | 5+Kar Modifier  |
| 8     | +5      | +6      | +4      | +7      | BS Point      | 6+Kar Modifier  |
| 9     | +5      | +7      | +5      | +8      | Formskift, Speciale      | 7+Kar Modifier  |
| 10    | +6      | +8      | +6      | +9      | Formskift, BS Point, Speciale     | 8+Kar Modifier  |

## Goodberries

Hver morgen kan Månesigeren fortrylle diverse bær og lave dem til
Goodberries. Goodberries helbreder 1 HP pr bær, Månesigeren kan lave 10
Goodberries pr level, til et max af 100 bær i level 10.

Denne evner kræver at Månesigeren er i besiddelse af bær.

## Faerie aura

I level 1 i kan en Månesiger Fjerne usynligheds effekter fra en
modstander og reducerer deres RK med 1 pr Månesiger level. Dette kan
gøres et antal gange om dagen svarende til Månesigerens Karisma
Modifier.

## Dyreven

Grundet Månesigerens tætte forhold til naturen og alt dyreliv kan de
vælger fra level 1 om de ønsker at have en familiar(kæledyr) med sig.

En familiar giver en særlig bonus baseret på hvilket dyr man vælger.

Listen over Familiars kan findes her: (Indsæt link)

## Formskift

Fra Level 1 kan Månesigeren med sin stærke forbindelse til naturen
skifte form til et dyr. Hvert dyr har særlig egenskaber og bonusser men
for at skifte form til et givent dyr, må månesigeren først vælge
aspektet fra det dyr han/hun vil skifte form til.

kun et aspekt kan vælges indtil Månesigeren rammer level 4 og kan dermed
vælge endnu et aspekt

Et nyt aspekt bliver igen tilgængeligt i level 6 og i level 9.

### Bjørnens Aspekt

Månesigeren skifter form til en medium stor brun
bjørn og får +6 i styrke samt konstitution.     
                                                
Derudover kan han/hun bruge evnen Vildskab som e
Hurtig handling dette giver månesigeren +2 til  
angrebs tjeks og evnen Intimiderende Brøl.      
Modstanden slår et Viljestyrketjek, fejler den   
tjekket bliver de skræmt i 1 runde +            
Karisma-modifer og tør ikke angribe Månesigeren 
mens han/hun er i bjørneform.                   
Klarer de tjekket bliver de provokeret og går   
specifikt efter Månesigeren i 1 runde + Karisma 
modifier. Viljestyrke Tjekket slås mod          
Månesigerens Besværgelses Niveau.               


### Ørnens Aspekt

Månesigeren skifter form til en lille ørn og får 
evnen til at flyve over ufremkommeligt terræn.   

### Egernets Aspekt

Månesigeren skifter form til et lille egern som 
giver dem evnen til at kunne klatrer op i træer.
                                                
Som egern får månesigeren en bevægeligheds bonus
på +10.                                         


### Frøens Aspekt
             
 Månesigeren skifter form til en lille frø som    
 giver dem evnen til at kunne holde vejret under  
 vand i ekstra lang tid og de får 4 meters ekstra 
bevægelse når de svømmer i denne form.            

### Tigerens Aspekt

Månesigeren skifter form til en medium tiger og  
får +2 i styrke og +3 i bevægelighed.            
Tigeren kan lave et Spring fra 3 meters afstand  
som giver 1d4 i skade og gør modstanderen døsig i
1 runde.                                         
Når man slår et angrebs tjek med Spring skal man 
lægge sin bevægeligheds modifier til slaget i    
stedet for Styrke.                               
Tigeren har også et bid som påfører fjenden      
"inficeret bid"- en sygdoms effekt som kræver et 
Kropsstyrke tjek svarende til Månesigerens       
Besværgelses Niveau, hvis det fejles tager       
modstanderen 2 skade hver runde indtil de er     
helbredt eller død.                              


## Dyresprog

I level 2 kan Månesigeren kommunikere med alt dyreliv i skovegne, de kan
snakke med og forstå dyrene og kan udnytte denne evne til at få
information fra dyr. Kræver karisma tjek.

## Grønne hænder
Månesigerens kendskab til naturen giver dem evnen til at kunne
genkende stort set alle planter, derudover kan de få næsten alle planter
til at vokse og modnes hurtigere end normalt.
I level 5 Du får en bonus på +6 til alle Viden Natur tjeks.


## Levende skov

Månesigeren udsiger en bøn til naturen og kalder på hjælp, dette kan
resultere i, at 2
Levende Træer springer frem fra omgivelserne og hjælper Månesigeren i
kamp.  Denne evne tager en runde at kaste og kan kun bruges i områder
med træer.
Et Levende Træ lever i 1 runde + level eller indtil den slås ihjel.
Denne evne kræver et Karisma tjek svarende til Månesigerens Besværgelses
Niveau.
