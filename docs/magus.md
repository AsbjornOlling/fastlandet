# Magus

![TODO: Skift det her billede ud](img/temp.png)

En Magus er en magiker fra Djinslægten som udover at beherkse magien
også er trænet i nærkamp. De er adrætte og elegante krigere og de
primære fodsoldater i Djinriget.
En Magus, benytter sig oftest af Safir ædelstenen, da den har egenskaber
som kan styrke krop og atletik.

De sætter stor lid til styrke og intelligens, for at de både kan mestre
våbenkamp og magi, men også bevægelighed er vigtig for en Magus, da de
sjældent bærer pladerustninger eller andet tungt udstyr.

**Moralsk Overbevisning:** Kaotisk God, Lovfuld God, Neutral eller Neutral God.

**HP Terning:** 1d8

**Klasse Kompetencer:**

Magussens klassekompetencer er Akrobatik (Bevæg),
klatrer(styr), Artisaner(Fortryllelse), Flugtmester(Bevæg), Intimidér
(Kar), Viden (historie og Magi) (Int), Ride (Bevæg),
List(Bevæg) og Svømme (Styr) Detekt Magi (Int)

## Magus tabel

Level | Angrebs Bonus | Viljestyrke | Refleks | Kropstyrke | Særlige Egenskaber                                     | Daglige Besværgelser |
----- | -------       | -------     | ------- | -------    | -------                                                | -------              |
1     | +1            | +2          | +2      | +2         | Ædelstensskole Acanisk Energi                          | 0+Int Modifier       |
2     | +2            | +2          | +2      | +2         | Acanisk Slag                                           | 1+Int Modifier       |
3     | +3            | +3          | +3      | +3         | Speciale Acanisk Energi+1                              | 2+Int Modifier       |
4     | +4            | +4          | +4      | +4         | BS Point Acanisk Meditation Ædelstensskole             | 3+Int Modifier       |
5     | +5            | +4          | +4      | +4         | Speciale Acanisk Energi +1                             | 3+Int Modifier       |
6     | +5            | +5          | +5      | +5         |                                                        | 4+Int Modifier       |
7     | +6            | +5          | +6      | +5         | Speciale Acanisk Energi +1                             | 4+Int Modifier       |
8     | +7            | +6          | +6      | +6         | BS Point                                               | 5+Int Modifier       |
9     | +8            | +6          | +7      | +6         | Acanisk Energi +1                                      | 6+Int Modifier       |
10    | +9            | +7          | +8      | +7         | Speciale, BS Point. Magisk Kyndighed Acanisk Energi +1 | 7+Int                |

**
## Særlige Egenskaber

### Spydmester

Magussen får automatisk fra level 1 kamp specialet Spyd mester.
Denne evne giver Magikeren +2 til alle angrebs tjeks med "rækkevidde" våben.
Herunder spyd, Glavind, Dobbeltsidet sværd, Bastad-sværd, helbards.
Når Magikerens AB rammer 4 øges bonussen med 2.

### Ædelstensskole

I level 1 har Magussen adgang til 1 af de 4 Ædelstensskoler, det
anbefales dog at man vælger Safir skolen, da den er bedst brugt hos
Magussen. I level 4 får Magussen adgang til én ny Ædelstensskole, men
modsat en Kazem, så tæller Magussens level som værende 1 når han vælger
den nye skole.

### Acanisk Energi

I level 1 har Magussen adgang til et reservoir af Acanisk Energi. Denne
Acaniske Energi kan Magussen indgyde på sit våben som en Hurtig
Handling. Dette giver våbenet +1 til Angrebs tjeks og skade. Effekten af
Acanisk Energi forøges hvert andet level med +1 og vare i 1 time.
En Magus kan bruge Acanisk Energi sammen antal gange dagligt svarende
til hans Intelligens Modifier + Halve Level.

### Acanisk Slag

I level 2 har Magussen evnen til at indgyde sit våben med en af sine
daglige berørings besværgelser. Dette gør at Magussen kan kaste sin
besværgelse gennem sit våben samtidigt med at han/hun angriber med
våbnet på et succesfuldt angrebs tjek.

### Acanisk Meditation

I level 4 kan Magussen gendanne et antal Besværgelser ved
at meditere i 1 time, dette tæller som et Kort Hvil. Antallet af
Besværgelser som kan gendannes er baseret på Magussen ubrugte Acanisk
Energi.

### Bonus Kamp Speciale

I level 5 får Magussen et ekstra Kamp Speciale.

### Magisk Kyndighed

Ligesom med en Kazem opnår Magussen magisk kyndighed når
de kommer i level 10. Dette gør at de kan vælge en ny Ædelstensskole og
bruge alle Besværgelserne derfra.
