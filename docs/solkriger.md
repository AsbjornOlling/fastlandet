# Solkriger

![TODO: Skift det her billede ud](img/temp.png)

Solkrigen er Solfolkets retfærdige og modigste kriger, de er trænet og
øvet i nærkæmp med skarpslidte våben og kan, om nødvendigt, hidkalde
evner fra deres Guddom, Sol.
Solkrigeren har brugt år på at træne og bedregøre deres Solevner til
kamp og er derfor ikke brugt eller øvet i at helbrede sår eller
sygdomme, de kan, men er langt mindre effektive end f.eks. en
mirakelmager.
Deres primære funktion er at stå i frontlinjen som en inspirerende
lyskilde for deres kammerater i kamp. Det vigtigste for en Solkriger er
deres Tro på Sol og deres Ed som de afgiver for et nobelt formål.
Selv-buffs og skade er deres specielle, Styrke og Tro er vigtigst for en
Solkriger

**Moralsk overbevisning:** Lovfuld God eller Lovfuld Ond

**HP Terning**: 1d10

## Klassekompetencer

Solkrigerens klassekompetencer er 

- Håndværk (Våbensmed (Int)
- Diplomati (Kar)
- Førstehjælp (Int)
- Viden (Religion) (Int)
- Ride(bevæg)
- Svømning(Styr)

## Solkriger Tabel

Level   | Angrebs Bonus | Viljestykre | Refleks | Kropsstyrke | Særlige egenskaber   | Daglige besværgelser
------- | -------       | -------     | ------- | -------     | ---                  | ----
1       | +2            | +2          | +0      | +1          | Sol Ed, Solsegl      | 0+Tros Modifier
2       | +3            | +2          | +1      | +1          |                      | 1+Tros Modifier
3       | +3            | +3          | +1      | +2          | Speciale, Nådegave   | 2+Tros Modifier
4       | +4            | +3          | +2      | +3          | BS Point, Solsegl +1 | 3+Tros Modifier
5       | +5            | +4          | +3      | +3          | Speciale             | 3+Tros Modifier
6       | +6            | +4          | +3      | +4          | Nådegave             | 4+Tros Modifier
7       | +7            | +5          | +4      | +4          | Speciale, Solsegl +1 | 5+Tros Modifier
8       | +8            | +6          | +4      | +5          | BS Point             | 5+Tros Modifier
9       | +9            | +7          | +5      | +6          | Speciale, Nådegave   | 6+Tros Modifier
10      | +10           | +8          | +6      | +7          | Speciale, Solsegl+1  | 7+Tros Modifier


## Særlige Egenskaber

### Sol Ed

Som solkriger vælger du en ud af 4 forskellige Ede at følge, denne ed
vil forme måden hvorpå din karakter handler i verden og give dig
forskellige bonusser.

De 4 Ede er:

**Kongens Ed:** Med denne Ed har Solkrigeren viet sit liv til sin konge
(Kong Erik Morgenstjerne eller en af hans to børn kronprins Jonathan
Morgenstjerne eller Prinsesse Freja Morgenstjerne)
Som Solkriger af Kongens Ed vil du gøre alt for at opretholde orden og
fred i kongens land og gøre alt i din magt for at beskytte tronfølgerens
retmæssige krav på tronen. Enhver som udviser mistillid, mangel på
respekt eller lovløshed mod kongeslægten er din fjende.
Du får +4 til alle diplomati og intimidér tjeks når du taler med
Solfolk.

**Kirkens Ed:** Med denne Ed har Solkrigeren viet sit liv til Solkirken
"Omnia Sol Conspiratio" og vil til enhver tid følge de retsninglinjer og
befalinger som kommer fra Kirkens menighed.
En Solkriger af Kirkens Ed er trofast, rettroende og loyal overfor
enhver af kirkens medlemmer.
Du får +4 til alle Tros tjek og du kan til enhver tid søge asyl i en
Solkirke, kloster eller kapel.

**Hertugens Ed:** Med denne Ed har Solkrigeren viet sit liv til en
hertug eller hertuginde, det vil sige en af de noble familier i
Solriget. Underlagt denne Ed vil Solkrigeren altid beskytte sin hertug
og hans ejendom, alle som truer hertugen, selv kongefamilien, er objekt
for din vrede.
Du får +4 til alle Videns tjek som omhandler Nobiliteter.

**Folkets Ed:** Med Denne Ed har Solkrigeren viet sit liv til Solfolket
og ikke blot en enkelt person eller institution. Som Underlagt denne Ed
vil Solkrigeren altid kæmpe for de svageste og mest udsatte mennesker
blandt Solfolket og sætter en lid til bekæmpe undertrykkelse og
udnyttelse af menige mænd og kvinder.
Du får +4 til alle Videns tjek som omhandler det lokale byliv i hele
Solriget.

## Våben og Rustnings Træning

En Solkriger er trænet i at bruge medium og tung rustning samt alle
slags simple nærkamps våben og skjold.

### Solaura

En solkriger har til enhver tid, medmindre han/hun ikke ønsker det, en
solaura omkring sig, denne aura reducerer alle slag som rammer
solkrigeren med 2 skade.
Denne aura påvirker kun Solkrigeren. Effekten af Solaura'en forsøges med
+2 Hvert 3 level. 3,5,7 og 9 til en samlet skadesreduktion på 10 i level 10.


### Solsegl

Solkrigeren kan som en "Hurtig Handling" påfører sig selv et af fire
Solsegl. Et segl er aktivt i 1 runde.

Segl                     | Effekt
---                      | ---
**Solsegl Konstitution** | Solkrigeren får +1 I Konstitution i 1 runde+ level
**Solsegl Velsignelse**  | Solkrigeren velsignes 1 runde+ level og får +1 i Tro
**Solsegl Styrke**       | Solkrigeren får +1 I Styrke i 1 runde+ level
**Solsegl Prædiken**     | Solkrigeren får +1 i Karisma 1 runde+ level

**Specialet Rettroende forstærker hvert segl med ½ af Solkrigerens Tros Modifier.**

### **Smite**

En Solkriger kan vælge at opbruge sit Solsegl for at kaste Smite mod en
fjende.
Dette gør at Solkrigeren må tilføje sin Tros modifier på alle angrebs
tjeks mod denne fjende.
Er fjenden udød eller en vederstyggelighed må Solkrigeren også tilføje
sin Tros modifier til skaden.
Effekten af Smite være ved indtil fjenden er besejret.

### Hellig Hånd

I level 2 får Solkrigeren evnen til at kunne kanalisere Sols lys til en
helbredende berøring. Solkrigeren kan helbrede sig selv eller en
allieret for 1d6 HP.
En Solkriger kan bruge denne evne et antal gange om dagen svarende til
deres ½ level + Tros modifier. Evnens effekt fordobles hvert andet level
dvs. 4,6.8 og 10 til en samlet max på 6d6 i level 10.
Denne evne fungerer som en almindelig handling når den bruges på andre,
men som en hurtig handling når Solkrigeren kaster den på sig selv.
Denne evne kan også bruges til at skade udøde og vederstyggeligheder og
fungerer således som en berørings besværgelse angreb.
**Specialet Rettroende:** Bærer Solkrigeren et helligt symbol, kan de
lægge deres Tros modifier til helbredelser og skade.

### Solnåde

I level 3 kan Solkrigeren vælge mellem en række nådegaver som automatisk
påføres alle som helbredes af Solkrigerens Hellig Hånd besværgelse.
Solkrigeren kan vælge 1 nådegave pr 3 level. Dsv. 3,6,9.

**Solnåde Frygtløshed**: Den påvirkede rammes af frygtløshed og bliver
immun overfor alle frygt effekter i antal runder svarende til ½
Solkrigerens level.

**Solnåde** **Vilje**: Den påvirkede bliver velsignet med Sols vilje og
får +4 til alle udholdenheds tjeks i antal runder svarende til ½
Solkrigerens level.

**Solnåde** **Renhed**: Den Påvirkede bliver renset for alle fysiske
lidelser og sygdomme, alle sygdoms effekter og forgiftninger forsvinder
og den påvirkede bliver immun i antal runder svarende til ½ Solkrigerens
level.
 
**Solnåde** **Sejr**: Den påvirkede bliver inspireret til at sejre og
fører sig frem i kamp med fornyet styrke han/hun får +4 til angrebs
tjeks i antal runder svarende til ½ Solkrigerens level.
 
**Solnåde** **Bastion**: En lysende aura spredes om den påvirkede og
han/hun bliver beskyttet af
Sols hellige lys, han/hun får +4 til sin RK i antal runder svarende til
½ Solkrigerens level.

I level 6 udvides arsenalet af nådegaver

**Solnåde Held:** Den påvirkede får ét ekstra slag til et valgfrit
tjek.

**Solnåde afholdenhed:** Den påvirkede bliver immun for alle
fortryllelses eller forførelses effekter i antal runder svarende til ½
Solkrigerens level.
 
**Solnåde Sindsro:** Den påvirkede bliver immun over for alle "Sinds
ændrende" effekter i antal runder svarende til Solkrigerens ½ level.

## Besværgelser
I level 3 har Solkrigeren adgang til alle level 1-3 besværgelser fra
Sollys magikskolen.

### Guddommeligt helbred
I level 5 kan Solkrigeren tilføje sit level til sin max HP og han får +4
til alle kropsstyrke tjeks.

### Sols Vrede
I level 8 får Solkrigeren evnen til at storme mod en fjende med
retfærdig vrede, dette gør at Solkrigeren må lægge sin Tros-modifier til
sit angrebs tjek samt de får en SR(Skades reduktion) på 15 mod alle
fysiske angreb fra fjenden de stormede imod i 1 runde + Tros-modifier.
