# Kazem

![TODO: Skift det her billede ud](img/temp.png)

En Kazem er en dygtig magiker fra Djinslægten som benytter sig af rå
magisk energi, denne energi findes i alt lys men kan spejles og styres
ved hjælp af ædelstene. En Kazem har længe studeret ædelstenenes
egenskaber og potentiale i forhold til at kontrollere magiske energier.
En Kazem er den som er mest lært i denne ædle og faglige kunst.
Ædelstene findes i 4 former, Ametyst, Smaragd, Safir og Rubin.
Hver af ædelstene repræsenterer en skole inden for magi som Kazem har
eller kan lære at mestre.
De er kløgtige, intelligente, perceptive og observerende mennesker med
en stor nysgerrighed og vilje til at lære og studerer verden. De trives
bedst med høj intelligens.

**Moralsk overbevisning:** Kaotisk God, Lovfuld God, Neutral eller Neutral God.

**HP Terning:** 1d6

**Klasse Kompetencer**

Magikerens klasse kompetencer er Vurder Værdigenstande (Int), Artisaner
(Alkymi og Fortryllelse(int), Viden (Alt (Int), Lingvistik (Int) og
Detekt Magi (int)**,** Diplomati (Kar).

## Kazem Tabel


Level   | Angrebs Bonus | Viljestyrke | Refleks | Kropsstyrke | Særlige egenskaber                   | Daglige besværgelser
------- | -------       | -------     | ------- | -------     | -------                              |
1       | +0            | +2          | +0      | +0          | Magiskole                            | 1+Int Modifier
2       | +0            | +3          | +0      | +0          |                                      | 2+Int Modifier
3       | +1            | +4          | +1      | +1          | Speciale                             | 3+Int Modifier
4       | +1            | +4          | +1      | +1          | BS Point, Ny Magisk Kyndighed        | 4+Int Modifier
5       | +2            | +5          | +1      | +1          | Speciale                             | 5+Int Modifier
6       | +2            | +6          | +2      | +2          |                                      | 6+Int Modifier
7       | +3            | +6          | +2      | +2          | Speciale                             | 7+Int Modifier
8       | +3            | +7          | +3      | +3          | BS Point, Ny Magisk Kyndighed        | 8+Int Modifier
9       | +3            | +7          | +4      | +3          |                                      | 9+Int Modifier
10      | +4            | +8          | +4      | +4          | Speciale, BS Point, Magisk Kyndighed | 10+Int Modifier


## Særlige Egenskaber

### Ædelstensskole

I level 1 skal en Kazem vælge 1 ud af de 4 Ædelstens skoler som de
mestre, dette giver dem adgang til alle besværgelserne i den valgte
Ædelstensskole. For at kunne benytte sig af en Ædelstens skole skal man
have en intelligens Base Stat på minimum 15 og være i besiddelse af
Færdigheden Viden Magi.
Når først valget er truffet kan det ikke ændres.

### Bonus færdighed

### Viden (magi)

En Kazem er lært i magi og har en stor viden om den,
derfor får de i level 1 en naturligt bonus på +2 til alle Videns tjek
som omhandler magi. Denne bonus lægges til deres klasse kompetence
bonus.

### Acanisk Bånd

I level 1 kan en Kazem danne et magisk bånd til en genstand eller en
Familiar(kæledyr)
Dette bånd giver genstanden eller din familiar en særlig bonus. Læs
eventuelt under Familiar og Magiske genstande for mere information.

### Acanisk Mærke

I level 1 kan Magikeren placere et Acanisk mærke på en
modstander, alle magikerens angrebs tjeks som skal ramme modstanderens
Berørings RK får +1.
Denne evne stiger med +1 hvert level.

### Magisk Kyndighed

Grundet Kazemens store kyndighed indenfor magisk viden kan de i level 4
tilegne sig en ny Ædelstensskole som de kan trække magiske kræfter fra.
Kazem'en får adgang til alle besværgelser fra den valgte Ædelstensskole.
Endnu en Ædelstensskole kan læres i level 8 og endnu en i level 10.
Under normale omstændigheder når man vælger en ny magiskole skal man
tælle det som at man er i level 1 med den pågældende skole. Men en Kazem
kan starte i sit nuværende level i den valgte Ædelstensskole.

### Bonus Meta Magi Speciale

I level 6 får en Kazem et ekstra Meta Magi Speciale.

### Magiens Mester 

I level 10 kan en Kazem lægge halvt sit level til sine daglige besværgelser.
